# Introduction #

CCGUI is a program that helps the user to launch jobs in cluster computing infrastructures.

The basic goal is to support the Sun Grid Engine. LSF may be supported in the future.

# Dependencies #
The program is implemented as a bash script using the dialog program, so the only dependencies are the BASH shell and dialog.
